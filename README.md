
# Truss Bridge Simulation

To run, first install rust here: https://www.rust-lang.org/tools/install

Then, in this directory, run `cargo run`. You may also need to install sdl2-ttf, which is how text is rendered.
