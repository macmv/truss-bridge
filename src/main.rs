use sdl2::{
  event::Event,
  keyboard::Keycode,
  pixels::Color,
  rect::{Point, Rect},
  render::{Canvas, Texture},
  ttf::Font,
  video::Window,
};
use std::{
  f64::consts::PI,
  ops::{Add, Mul, Sub},
  time::Duration,
};

#[derive(Debug, Clone, Copy)]
struct Vec2 {
  pub x: f64,
  pub y: f64,
}

impl Vec2 {
  pub fn new(x: f64, y: f64) -> Self { Vec2 { x, y } }

  pub fn avg(&self, other: Vec2) -> Vec2 {
    Vec2 { x: (self.x + other.x) / 2.0, y: (self.y + other.y) / 2.0 }
  }
}

impl Into<Point> for Vec2 {
  fn into(self) -> Point { Point::new(self.x as i32, self.y as i32) }
}

impl Add for Vec2 {
  type Output = Self;

  fn add(self, rhs: Vec2) -> Self::Output { Vec2 { x: self.x + rhs.x, y: self.y + rhs.y } }
}
impl Sub for Vec2 {
  type Output = Self;

  fn sub(self, rhs: Vec2) -> Self::Output { Vec2 { x: self.x - rhs.x, y: self.y - rhs.y } }
}
impl Mul<f64> for Vec2 {
  type Output = Self;

  fn mul(self, rhs: f64) -> Self::Output { Vec2 { x: self.x * rhs, y: self.y * rhs } }
}

struct Render<'a, 'b> {
  font:   &'a Font<'b, 'b>,
  canvas: &'a mut Canvas<Window>,
}

impl Render<'_, '_> {
  fn draw_text(&mut self, pos: Vec2, text: &str) {
    let surface = self.font.render(text).solid(Color::RGB(255, 255, 255)).unwrap();
    let rect = Rect::new(pos.x as i32, pos.y as i32, surface.width(), surface.height());

    let texture_creator = self.canvas.texture_creator();
    let texture = Texture::from_surface(&surface, &texture_creator).unwrap();
    self.canvas.copy(&texture, None, Some(rect)).unwrap();
  }
  fn draw_arrow(&mut self, start: Vec2, end: Vec2) {
    self.canvas.draw_line(start, end).unwrap();
    self.canvas.draw_line(end + Vec2::new(5.0, -5.0), end).unwrap();
    self.canvas.draw_line(end + Vec2::new(-5.0, -5.0), end).unwrap();
  }
  fn draw_simulation(&mut self, angle: f64, force: f64) {
    //     A ----- B
    //    / \     / \
    //   /   \   /   \
    //  /     \ /     \
    // E ----- D ----- C

    // This is the upward force at C and E
    let up_force = force / 2.0;
    // This is the angle at E and C
    let wide_angle = (PI - angle) / 2.0;

    // These are the forces on each beam
    let f_ae = -up_force / wide_angle.sin();
    let f_de = -wide_angle.cos() * f_ae;
    let f_ad = -f_ae;
    let f_ab = (angle / 2.0).sin() * f_ae - (angle / 2.0).sin() * f_ad;

    let f_bd = f_ad;
    let f_bc = f_ae;
    let f_cd = f_de;

    // This is how the points get positioned on the screen.
    let base = Vec2::new(1.0, 1.0);
    let scale = 200.0;

    let a = base;
    let b = base + Vec2::new((angle / 2.0).sin() * 2.0, 0.0);
    let e = a + Vec2::new(-(angle / 2.0).sin(), (angle / 2.0).cos());
    let d = e + Vec2::new((angle / 2.0).sin() * 2.0, 0.0);
    let c = d + Vec2::new((angle / 2.0).sin() * 2.0, 0.0);

    // This is how all the points get labels in the resulting render.
    let points = [("A", a), ("B", b), ("C", c), ("D", d), ("E", e)];
    // This is how all the beams get labels in the resulting render.
    let lines = [
      (a, b, f_ab),
      (a, e, f_ae),
      (a, d, f_ad),
      (b, d, f_bd),
      (b, c, f_bc),
      (e, d, f_de),
      (d, c, f_cd),
    ];

    // This draws the current angle in the top-left of the screen.
    self.draw_text(Vec2::new(0.0, 0.0), &format!("{:.02}°", angle / PI * 180.0));

    // This draws all the points and their labels.
    for (name, point) in points {
      self.draw_text(point * scale, name);
    }

    // This draws all the beams and their labels.
    for (a, b, f) in lines {
      self.canvas.set_draw_color(Color::RGB((f * 255.0) as u8, 50, 50));
      self.canvas.draw_line(a * scale, b * scale).unwrap();

      let center = a.avg(b) * scale;

      self.draw_text(center, &format!("{f:.02}"));
    }

    self.draw_arrow(d * scale + Vec2::new(0.0, 10.0), d * scale + Vec2::new(0.0, 20.0));
    self.draw_text(d * scale + Vec2::new(0.0, 25.0), &format!("{force:.02}N"));
  }
}

pub fn main() {
  let sdl_context = sdl2::init().unwrap();
  let video_subsystem = sdl_context.video().unwrap();

  let window =
    video_subsystem.window("truss-bridge", 800, 600).position_centered().build().unwrap();

  let mut canvas = window.into_canvas().build().unwrap();

  let mut angle = 60.0_f64 / 180.0 * PI;
  let force = 1.19_f64;

  let fonts = sdl2::ttf::init().unwrap();
  let font = fonts.load_font("/usr/share/fonts/TTF/DejaVuSans.ttf", 20).unwrap();

  canvas.set_draw_color(Color::RGB(50, 50, 50));
  canvas.clear();
  canvas.present();
  let mut event_pump = sdl_context.event_pump().unwrap();
  'running: loop {
    canvas.set_draw_color(Color::RGB(50, 50, 50));
    canvas.clear();
    for event in event_pump.poll_iter() {
      match event {
        Event::Quit { .. } | Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
          break 'running
        }
        Event::KeyDown { keycode: Some(Keycode::Minus), .. } => {
          angle -= 5.0 / 180.0 * PI;
        }
        Event::KeyDown { keycode: Some(Keycode::Equals), .. } => {
          angle += 5.0 / 180.0 * PI;
        }
        _ => {}
      }
    }

    {
      let mut render = Render { font: &font, canvas: &mut canvas };
      render.draw_simulation(angle, force);
    }

    canvas.present();
    ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
  }
}
